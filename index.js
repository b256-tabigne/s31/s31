const http = require("http")

const port = 3000

const server = http.createServer(function (req, res)
	{
		if(req.url == "/login") 
			{
				res.writeHead(200, {'Content-Type': 'text/plain'})
				res.end("Welcome to the login page.")
			}
		else
			{
				res.writeHead(404, {'Content-Type': 'text/plain'})
				res.end("I'm sorry the page you are looking for cannot be found.")	
			}
	}
)

server.listen(port)

console.log(`Server is now accessible at localhost:${port}`)